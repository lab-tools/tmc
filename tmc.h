/*
 * tmc.h - Device-independent TMC functions
 *
 * Copyright (C) 2008 by OpenMoko, Inc.
 * Written by Werner Almesberger <werner@openmoko.org>
 * All Rights Reserved
 *
 * Continued 2014 by Werner Almesberger <werner@almesberger.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef TMC_H
#define TMC_H

#include <sys/types.h>

#include "io.h"


struct tmc_dsc;

struct tmc_dsc *tmc_open(const struct proto_ops *ops,
    int argc, const char **argv);
int tmc_close(struct tmc_dsc *dsc);
int tmc_send(struct tmc_dsc *dsc, int argc, const char **argv);
int tmc_read(struct tmc_dsc *dsc, void *buf, ssize_t size);
int tmc_start(struct tmc_dsc *dsc, const char *file, const char *repeat);
int tmc_stop(struct tmc_dsc *dsc);
int tmc_errors(struct tmc_dsc *dsc);

void tmc_debug(int level);

/*
 * This should probably go somewhere else ...
 */

const struct proto_ops *tmc_ops(const char *name);

#endif /* !TMC_H */
