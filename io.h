/*
 * io.h - I/O handler for SCPI responses
 *
 * Copyright (C) 2008 by OpenMoko, Inc.
 * Written by Werner Almesberger <werner@openmoko.org>
 * All Rights Reserved
 *
 * Continued 2014 by Werner Almesberger <werner@almesberger.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef IO_H
#define IO_H

#include <sys/types.h>
#include <sys/time.h>


typedef void io_push_fn(void *dsc, const struct timeval *tv,
    const void *buf, size_t len, int end);

void *io_setup_async(const char *file);
void io_push_async(void *dsc, const struct timeval *tv, const void *buf,
    size_t len, int end);
void io_end_async(void *dsc);

void *io_create_buf(void);
void io_push_buf(void *dsc, const struct timeval *tv, const void *buf,
    size_t len, int end);
size_t io_read_buf(void *dsc, void *buf, size_t size);
void io_cancel_buf(void *dsc);


struct proto_ops {
	void *(*open)(int argc, const char **argv); 
	void (*close)(void *dsc);
	int (*write)(void *dsc, const void *buf, size_t len);
	int (*read)(void *dsc, io_push_fn *push, void *push_dsc);
	int (*dci)(void *dsc);
	int (*errors)(void *dsc);
};


extern int debug;

extern struct proto_ops telnet_ops; /* TELNET-like */
extern struct proto_ops usbtmc_ops; /* USBTMC */
extern struct proto_ops tty_ops; /* TTY */

#endif /* !IO_H */
