/*
 * telnet.c - Send and receive SCPI messages over a TELNET (TCP) connection
 *
 * Copyright (C) 2008 by OpenMoko, Inc.
 * Written by Werner Almesberger <werner@openmoko.org>
 * All Rights Reserved
 *
 * Continued 2014 by Werner Almesberger <werner@almesberger.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * This code differs from generic handling of a data transfer over TCP in the
 * following ways:
 *
 * - adds CRLF when sending,
 * - strips CRLF when receiving,
 * - tried to connect multiple times if the connection is refused,
 * - reconnects and retransmits the last message if the connection is reset.
 *
 * The last two features are needed to reliably begin talking to a Fluke 8845A.
 * Once the session is established, no such anomalies have been observed.
 */

/*
 * Known issues:
 * - recovery logic may allow more retries than configured (need to check)
 * - retry delay should be configurable
 * - argument passing concept is a bit ugly
 */


#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>
#include <limits.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "io.h"


#define BUF_SIZE 1024

#define DEFAULT_PORT "3490"
#define DEFAULT_TRIES 1


struct telnet_dsc {
	int sd;			/* socket descriptor */
	int crlf;		/* 0 = in message, 1 = CR seen, 2 = CRLF seen */
	int nl;			/* 0 = complain about extra \n, 1 = don't */
	unsigned max_tries;	/* number of times we try to set up the
				   connection */
	unsigned tries_left;	/* number of attempts left */
	unsigned timeout_s;	/* timeout in seconds. 0 = infinite */
	unsigned reconnect_delay_s;
				/* delay between reconnect attempts,
				   default 1 s */
	unsigned dci_delay_s;	/* delay until flushing the buffer after
				   sending DCI, default 1 s */
	struct sockaddr_in addr;/* peer address */
	void *last_buf;		/* last buffer sent */
	int last_len;
	int errors;		/* reconnect attempts */
};


static void usage(void)
{
	fprintf(stderr,
	    "usage: \"telnet\", [tries=N,] [timeout=N,] [reconnect_delay=N,]\n"
	    "      [dci_delay=N,] [nl,] host [, port]\n");
}


static int do_write(int sd, const void *buf, size_t len)
{
	ssize_t wrote;

	while (len) {
		wrote = write(sd, buf, len);
		if (wrote < 0) {
			perror("write");
			return -1;
		}
		if (!wrote) {
			fprintf(stderr, "write: wrote 0 bytes\n");
			return -1;
		}
		buf += wrote;
		len -= wrote;
	}
	return 0;
}


static int telnet_dci(void *dsc)
{
	struct telnet_dsc *d = dsc;
	char buf[BUF_SIZE];
	int res;

	res = do_write(d->sd, "\003", 1);
	if (!res) {
		sleep(d->dci_delay_s);
		(void) recv(d->sd, buf, sizeof(buf), MSG_DONTWAIT);
	}
	return res;
}


static int do_connect(struct telnet_dsc *d)
{
	d->sd = socket(PF_INET, SOCK_STREAM, 0);
	if (d->sd < 0) {
		perror("socket");
		return -1;
	}

	while (d->tries_left || !d->max_tries) {
		if (d->tries_left)
			d->tries_left--;
		d->errors++;
		if (connect(d->sd,
		    (struct sockaddr *) &d->addr, sizeof(d->addr)) >= 0)
			return 0;
		if (errno != ECONNREFUSED || !d->tries_left) {
			perror("connect");
			break;
		}
		sleep(d->reconnect_delay_s);
	}
	if (close(d->sd) < 0)
		perror("close");
	return -1;
}


static int parse_unsigned(const char *name, const char *arg, unsigned *res)
{
	int len = strlen(name);
	unsigned long tmp;
	char *end;

	if (strncmp(name, arg, len))
		return 0;
	if (arg[len] != '=')
		return 0;
	tmp = strtoul(arg+len+1, &end, 0);
	if (*end)
		return -1;
	if (tmp > UINT_MAX)
		return -1;
	*res = tmp;
	return 1;
}


static void *telnet_open(int argc, const char **argv)
{
	struct telnet_dsc *d;
	const char *host, *port;
	const struct hostent *he;
	const struct servent *se;
	unsigned long num;
	char *end;
	int i, res;

	d = malloc(sizeof(struct telnet_dsc));
	if (!d) {
		perror("malloc");
		return NULL;
	}
	d->max_tries = DEFAULT_TRIES;
	d->nl = 0;
	d->timeout_s = 0;
	d->reconnect_delay_s = 1;
	d->dci_delay_s = 1;

	for (i = 0; i != argc; i++) {
		res = parse_unsigned("tries", argv[i], &d->max_tries);
		if (res < 0)
			goto usage;
		if (res)
			continue;

		if (!strcmp(argv[i], "nl")) {
			d->nl = 1;
			continue;
		}

		res = parse_unsigned("timeout", argv[i], &d->timeout_s);
		if (res < 0)
			goto usage;
		if (res)
			continue;

		res = parse_unsigned("reconnect_delay", argv[i],
		    &d->reconnect_delay_s);
		if (res < 0)
			goto usage;
		if (res)
			continue;

		res = parse_unsigned("dci_delay", argv[i], &d->dci_delay_s);
		if (res < 0)
			goto usage;
		if (res)
			continue;

		break;
	}
	if (i == argc || i-argc > 2)
		goto usage;
	host = argv[i];
	port = argc > i+1 ? argv[i+1] : DEFAULT_PORT;

	d->crlf = 0;

	d->addr.sin_family = AF_INET;
	d->addr.sin_addr.s_addr = inet_addr(host);
	if (d->addr.sin_addr.s_addr == INADDR_NONE) {
		he = gethostbyname(host);
		if (!he) {
			fprintf(stderr, "unknown host \"%s\"\n", host);
			goto fail;
		}
		d->addr.sin_addr = *(struct in_addr *) he->h_addr;
	}

	se = getservbyname(port, "tcp");
	if (se)
		d->addr.sin_port = se->s_port;
	else {
		num = strtoul(port, &end, 0);
		if (!num || num > 0xffff || *end) {
			fprintf(stderr, "invalid port \"%s\"\n", port);
			goto fail;
		}
		d->addr.sin_port = htons(num);
	}
	
	d->tries_left = d->max_tries;
	d->last_buf = NULL;
	d->errors = -1;
	if (!do_connect(d))
		return d;

usage:
	usage();
fail:
	free(d);
	return NULL;
}


static void telnet_close(void *dsc)
{
	struct telnet_dsc *d = dsc;

	if (close(d->sd) < 0)
		perror("close");
	if (d->last_buf)
		free(d->last_buf);
}


static int telnet_write(void *dsc, const void *buf, size_t len)
{
	struct telnet_dsc *d = dsc;
	void *tmp;

	tmp = malloc(len);
	if (!tmp) {
		perror("malloc");
		return -1;
	}
	memcpy(tmp, buf, len);
	if (d->last_buf)
		free(d->last_buf);
	d->last_buf = tmp;
	d->last_len = len;

	/* "buf" may be d->last_buf, so use the copy now */
	if (do_write(d->sd, tmp, len) < 0)
		return -1;
	return do_write(d->sd, "\r\n", 2);
}


static int telnet_read(void *dsc, io_push_fn *push, void *push_dsc)
{
	struct telnet_dsc *d = dsc;
	struct pollfd pollfd = {
		.fd     = d->sd,
		.events = POLLIN | POLLHUP | POLLERR,
	};
	char buf[BUF_SIZE];
	struct timeval tv;
	int fds;
	ssize_t got;
	char *p;

	do {
		fds = poll(&pollfd, 1, d->timeout_s ? d->timeout_s*1000 : -1);
		if (fds < 0) {
			perror("poll");
			exit(1);
		}
		if (fds)
			got = read(d->sd, buf, BUF_SIZE);
		if (!fds || (got < 0 && errno == ECONNRESET)) {
			if (close(d->sd) < 0)
				perror("close");
			if (do_connect(d) < 0)
				return -1;
			if (telnet_dci(d))
				continue;
			if (telnet_write(dsc, d->last_buf, d->last_len) < 0)
				return -1;
			continue;
		}
		if (got < 0) {
			perror("read");
			return -1;
		}
		if (!got) {
			fprintf(stderr, "read: unexpected EOF\n");
			return -1;
		}
		if (gettimeofday(&tv, NULL) < 0) {
			perror("gettimeofday");
			return -1;
		}
		d->tries_left = d->max_tries;
		for (p = buf; p != buf+got; p++)
			switch (*p) {
			case '\r':
				if (d->crlf)
					fprintf(stderr,
					    "warning: unexpected \\r\n");
				d->crlf = 1;
				break;
			case '\n':
				if (d->crlf != 1 && !d->nl)
					fprintf(stderr,
					    "warning: unexpected \\n\n");
				d->crlf = 2;
				break;
			default:
				if (d->crlf)
					fprintf(stderr,
					    "warning: data after \\r\\n\n");
				d->crlf = 0;
				break;
			}
		if (d->crlf <= got)
			push(push_dsc, &tv, buf, got-d->crlf, d->crlf == 2);
	}
	while (d->crlf != 2);
	d->crlf = 0;
	return 0;
}


static int telnet_errrors(void *dsc)
{
	struct telnet_dsc *d = dsc;

	return d->errors;
}


struct proto_ops telnet_ops = {
	.open	= telnet_open,
	.close	= telnet_close,
	.read	= telnet_read,
	.write	= telnet_write,
	.dci	= telnet_dci,
	.errors	= telnet_errrors,
};
