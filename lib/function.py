#
# function.py - Function generator control
#
# Copyright (C) 2008 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


from tmc.instrument import instrument, setting


class function(instrument):
    # Warning: "function" doesn't lock itself !
    pass


class picotest_g5100a(function):

    def __init__(self, name = None):
	if name is None:
	    function.__init__(self, "usbtmc", "timeout=2", "retry",
	      "vendor=0x164e", "product=0x13ec")
	else:
	    function.__init__(self, "telnet", "tries=3", "nl", name, "5025")
	self.lock_attr()

    def read(self):
	return function.read(self).rstrip('\n')
