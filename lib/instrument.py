#
# instrument.py - General instrument infrastructure
#
# Copyright (C) 2008 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#
# Continued 2014 by Werner Almesberger <werner@almesberger.net>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Special thanks to Guillaume "Charlie" Chereau for his assistance with the
# get/set functionality.
#


import _tmc


class settings(object):
    def __init__(self, init, set = None, get = None):
        self.value = init
        self.__set = set
        self.__get = get

    def get(self):
        if self.__get is None:
            return self.value
        else:
            value = self.__get()
	    self.value = value
	    return value

    def set(self, value):
        if self.__set is not None:
            self.__set(value)
        self.value = value


class setting(settings):

    def __init__(self, instr, path, in_convert = None, out_convert = None,
      args = "", get_cmd = None, set_cmd = None):
	self.value = None
	self.instr = instr 
	self.path = path
	self.in_convert = in_convert
	self.out_convert = out_convert 
	self.args = args 
	self.get_cmd = get_cmd
	self.set_cmd = set_cmd

    def get(self):
	if self.value is None:
	    self.value = self.instr.query(
	      (self.get_cmd, str(self.path)+"?")[self.get_cmd is None]+
	      (" "+self.args, "")[self.args == ""])
	    if self.in_convert is not None:
		self.value = self.in_convert(self.value)
	return self.value

    def set(self, value):
	if self.path is None and self.set_cmd is None:
	    raise hell
	if value is None:
	    self.value = None
	elif value != self.value:
	    self.value = value
	    if self.out_convert:
		value = self.out_convert(value)
		if value is None:
		    raise hell
	    self.instr.send((self.set_cmd, self.path)[self.set_cmd is None]+
	      " "+self.args+str(value))


#
# The attributes of a "settable" object can be locked. This means that any
# attempt to create a new attribute will cause an AttributeError. This is to
# protect against accidently adding new attributes, which would otherwise cause
# a silent failure.
#
# In order to avoid having to call settable.__init__ in the __init__ methods of
# all subclasses, attr_is_locked also considers the absence of the
# "attr_locked" attribute to mean that the class is not (yet) locked.
#

class settable(object):

    def set(self, **list):
	for key in list.keys():
	    self.__setattr__(key, list[key])

    def attr_is_locked(self):
	try:
	    return super(settable, self).__getattribute__("attr_locked")
	except AttributeError:
	    return False

    def lock_attr(self):
	if self.attr_is_locked():
	    raise hell
	object.__setattr__(self, "attr_locked", True)

    def unlock_attr(self):
	if not self.attr_is_locked():
	    raise hell
	object.__setattr__(self, "attr_locked", False)

    def __getattribute__(self, name):
	ret = super(settable, self).__getattribute__(name)
	if isinstance(ret, settings):
	    return ret.get()
	return ret

    def __setattr__(self, name, value):
	attr = self.__dict__.get(name)
	if isinstance(attr, settings):
	    return attr.set(value)
	if self.attr_is_locked():
	    super(settable, self).__getattribute__(name)
	return super(settable, self).__setattr__(name, value)


#
# For some reason, we can't subclass _tmc.Instr, so we just redefine the few
# methods is has. @@@FIXME
#

class instrument(settable):

    debug_default = True

    def __init__(self, *args):
	self.__tmc = _tmc.Instr(*args)
	self.debug = self.debug_default
	# Warning: "instrument" doesn't lock itself !

    def send(self, s):
	if self.debug:
	    print "SEND", s
	self.__tmc.send(s)

    def read(self):
	res = self.__tmc.read()
	if self.debug and len(res) < 100:
	    print "RECV", res
	return res

    def start(self, file, command = None):
	if command is None:
	    self.__tmc.start(file)
	else:
	    self.__tmc.start(file, command)

    def stop(self):
	self.__tmc.stop()

    def errors(self):
	return self.__tmc.errors()

    def query(self, s):
	self.send(s)
	return self.read()

    def identify(self):
	return self.query("*IDN?")
