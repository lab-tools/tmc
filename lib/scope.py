#
# scope.py - Oscilloscope control
# 
# Copyright (C) 2008 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


# Found Rigol screen dump algorithm here:
# http://www.circuitsonline.net/forum/view/message/652573#652573
#
# Rigol data retrieval is heavily based on
# http://prive.bitwizard.nl/rigol/rigol-0.1rew3.tgz
#

#
# Our oscilloscope model consists of three elements:
#
# - one or more channels (vertical systems)
# - one horizontal system (the time base)
# - triggers
#

import time
from tmc.instrument import settings, setting, settable, instrument
from tmc.wave import analog, digital, waves
from tmc.trigger import trigger


# horizontal system only accepts some frequencies
# TODO: make range configurable

def scale_125(n = None, fuzz = None, min = None, max = None):
    epsilon = 1e-15
    res = None
    for e in range(-9, 1+1):
	for m in [1, 2, 5]:
	    if e < 0:
		s = "0."+"0"*(-e-1)+str(m)
	    else:
		s = str(m)+"0"*e
	    r = float(s)
	    if n is not None and r >= n*(1-fuzz) and r <= n*(1+fuzz):
		return s
	    if min is not None and r >= min-epsilon:
		return s
	    if max is not None and r <= max+epsilon:
		res = s
    if res is not None:
	return res
    raise hell


# TODO: figure out what to do with signals. should they be objects in their
# own right ?

class signal(object):
	# coupling, probe, filter, range

    def pos(self, value):
	pass

    def scale(self, value):
	pass

    def range(self, low, high, divs):
	self.pos((high+low)/2.0)
	self.scale((high-low)/divs)


# === channel =================================================================
#
# A channel is a vertical system of a scope. A scope can have any number of
# channels. Channel settings determine which part of the signal voltage is
# captured and at what bandwidth. Channels return the waveform captured.
#

class channel(settable):

    def __init__(self, scope, number):

	self.scope = scope
	self.number = number
	self.name = "CHAN"+str(number)

	self.on = setting(scope, ":CHAN"+str(number)+":DISP",
	  lambda on: on == "ON",
	  lambda on: ("OFF", "ON")[on])

	self.pos = setting(scope, ":CHAN"+str(number)+":OFFS",
	  lambda pos: -float(pos),
	  lambda pos: -pos)

	self.scale = setting(scope, ":CHAN"+str(number)+":SCAL",
	  lambda x: float(x))

	self.coupling = setting(scope, "???")

	self.probe = setting(scope, ":CHAN"+str(number)+":PROB",
	  out_convert = self.scope.probe_translate)

	self.bwlimit = setting(scope, ":CHAN"+str(self.number)+":BWL",
	  out_convert = self.scope.bwlimit_translate)

	self.lock_attr()
	self.forget()

    def forget(self):
	self.on = None
	self.pos = None
	self.scale = None
	self.coupling = None
	self.probe = None
	self.bwlimit = None


    # --- wave download -------------------------------------------------------

    # obsolete !

    def wave(self, start = None, end = None, step = None):
	# @@@ move start and end adjustment here !
	return self.scope.download_wave(self, start, end, step)


# === horizontal ==============================================================


class sweep:
    Auto, Normal, Single = range(3)


class state:
    Run, Stop, Triggered, Wait, Scan, Auto = range(6)


class horizontal_trigger_setting(settings):

    def __init__(self, scope, set = None, get = None):
	self.scope = scope
	self.trigger_set = set
	self.trigger_get = get
	settings.__init__(self, None, set = self.pass_set, get = self.pass_get)

    def pass_set(self, value):
	if self.scope.trigger is not None and self.trigger_set is not None:
	    self.trigger_set(self.scope.trigger, value)

    def pass_get(self):
	if self.scope.trigger is not None and self.trigger_get is not None:
	    return self.trigger_get(self.scope.trigger)


class horizontal(settable):

    state_map = [ "RUN", "STOP", "T'D", "WAIT", "SCAN", "AUTO" ]

    def __init__(self, scope):
	self.scope = scope
	self.pos = setting(scope, ":TIM:OFFS",
	  lambda x: float(x))
	self.scale = setting(scope, ":TIM:SCAL",
	  lambda x: float(x),
	  lambda x: x*0.99)
	# Rigol special: 0.0005 becomes 1ms, so we have to round down :-(
	self.sweep = horizontal_trigger_setting(scope,
	  trigger.set_sweep, trigger.get_sweep)
	self.lock_attr()
	self.forget()

    def forget(self):
	self.pos = None
	self.scale = None
	self.sweep = None

    # "run" and "stop" are experimental: should we instead just have a common
    # attribute "state" that then automatically determines what to do ? Also,
    # should "run" send a ":STOP" first ?

    def run(self):
	self.scope.send(":RUN")

    def stop(self):
	self.scope.send(":STOP")

    def force(self):
	self.scope.send(":FORC")

    def state(self):
	return self.state_map.index(self.scope.query(":TRIG:STAT?"))


# === Scope class =============================================================


class scope(instrument):
    # Warning: "scope" doesn't lock itself !
    pass


# === Rigol DS1000C scope =====================================================


#
# Experiments confirm that the verical center is really at 125, not 127 or 128
# as one might expect. The horizontal offsets have also been confirmed by
# experiment.
#
# The correct position of the horizontal center is more difficult to determine
# because what appears on the scope's screen as one vertical line are in fact
# two samples. With a vertical line at hor = 0, the transition occurs between
# offsets 512 and 513, making 512 a plausible choice.
#

def rigol_channel_data(s, t0, td, v0, vd):
    res = analog()
    i = 212
    while i != 812:
	div = (i-512)/50.0
	t = t0+div*td
	div = (125-ord(s[i]))/25.0
	v = v0+div*vd
	res.append(t, v)
	i += 1
    return res


def rigol_la_data(s, t0, td):
    res = []
    for b in range(0, 16):
	res.append(digital())

    prev_low = 0
    prev_high = 0

    i = 424
    while i != 1624:
	div = (i-1024)/100.0
	t = t0+div*td

	doit = i == 424 or i == 1622

	byte = ord(s[i])
	if byte ^ prev_low or doit:
	    prev_low = byte
	    for b in range(0, 8):
		res[b].append(t, (byte & (1 << b)) != 0)

	byte = ord(s[i+1])
	if byte ^ prev_high or doit:
	    prev_high = byte
	    for b in range(0, 8):
		res[b+8].append(t, (byte & (1 << b)) != 0)

	i += 2
    return res


def rigol_wave(scope, start, end, step, query, merge, fn, *args):
    wave = None
    orig_pos = scope.hor.pos
    orig_scale = scope.hor.scale

    width = scope.div_hor*orig_scale
    if start is None:
	start = orig_pos-width/2
    if end is None:
	end = orig_pos+width/2
    if step is None:
	step = 1/scope.sampling_rate()

    scope.hor.scale = float(scale_125(max = step*scope.samples_per_div))
    while start < end:
	scope.hor.pos = start+6*scope.hor.scale
	data = scope.query(query)
	data = fn(data, scope.hor.pos, scope.hor.scale, *args)
	if wave is None:
	    wave = data
	else:
	    merge(wave, data)
	start += 12*scope.hor.scale

    scope.hor.pos = orig_pos
    scope.hor.scale = orig_scale
    return wave


def rigol_extend_la(a, b):
    i = 0
    while i != 16:
	a[i].extend(b[i])
	i += 1


#
# Encoding is rrgggbbb, so we spread this into
#
# rr  * 0b01010101
# ggg * 0b01001001 >> 1
# bbb * 0b01001001 >> 1
#

def rigol_to_ppm(s):
    lut = []
    i = 0
    while i != 256:
	lut.append(chr((i >> 6)*0x55)+
	  chr((((i >> 3) & 7)*0x49) >> 1)+
	  chr(((i & 7)*0x49) >> 1))
	i += 1
    res = "P6 320 234 255\n"
    i = 0
    while i != 320*234:
	res += lut[ord(s[i])]
	i += 1
    return res


class rigol_ds1000c(scope):
    channels = 2
    div_hor = 12
    div_vert = 10
    samples_per_div = 50

    def __init__(self):
	scope.__init__(self, "usbtmc", "rigol", "timeout=2", "retry",
          "vendor=0x0400", "product=0x05dc")
	self.ch = []
	self.d = map(lambda x: x, range(0, 16))
	for n in range(1, self.channels+1):
	    self.ch.append(channel(self, n))
	self.trigger = None
	self.hor = horizontal(self)
	self.lock_attr()

    def forget(self):
	for ch in self.ch:
	    ch.forget()
	self.hor.forget()

    def send(self, s):
	scope.send(self, s)
	time.sleep(0.3)

    # --- functions below are for internal use --------------------------------

    def probe_translate(self, probe):
	return { 1:"1X", 10:"10X", 100:"100X", 1000:"1000X" }[probe]

    def bwlimit_translate(self, limit):
	return ("OFF", "ON")[limit <= 20*1.01]

    def trigger_source_translate(self, source):
	source = source.upper()
	if source == "CH1":
	    pass

    def download_wave(self, channel, start, end, step):
	return rigol_wave(self, start, end, step,
	  ":WAV:DATA? CHAN"+str(channel.number),
	  lambda a, b: a.extend(b),
	  rigol_channel_data, channel.pos, channel.scale)

    # experimental

    def download_la(self, start = None, end = None, step = None):
	return rigol_wave(self, start, end, step,
	  ":WAV:DATA? DIG",
	  rigol_extend_la, rigol_la_data)

    def sampling_rate(self):
	return float(self.query(":ACQ:SAMP? CH1"))

    def screendump(self):
	return rigol_to_ppm(self.query(":LCD:DATA?"))

    def wave(self, channels, start = None, end = None, step = None):
	if not hasattr(channels, "__iter__"):
	    return self.wave([channels], start, end, step)[0]
	la = None
	res = waves()
	for ch in channels:
	    if isinstance(ch, channel):
		res.append(self.download_wave(ch, start, end, step))
		res[-1].label = ch.name
	    else:
		if la is None:
		    la = self.download_la(start, end, step)
		res.append(la[ch])
		res[-1].label = "D"+str(ch)
	return res

    # WORK IN PROGRESS. INTERFACE WILL CHANGE !

    # DS1000C/CD only ! E/D will need a different map.
    #
    # Note that we don't check if 400MSa/s mode is actually available.

    rate_map = (
	( 400e6,  50e-9 ),	#  50ns/div -> 400MSa/s
	( 200e6, 200e-9 ),	# 200ns/div -> 200MSa/s
	( 100e6, 200e-6 ),	# 200us/div -> 100MSa/s, mind the gap !
	(  50e6, 500e-6 ),	# 500us/div ->  50MSa/s
	(  20e6,   1e-3 ),	#   1ms/div ->  20MSa/s
	(  10e6,   2e-3 ),	#   2ms/div ->  10MSa/s
	(   5e6,   5e-3 ),	#   5ms/div ->   5MSa/s
	(   2e6,  10e-3 ),	#  10ms/div ->   2MSa/s
	(   1e6,  20e-3 ),	#  20ms/div ->   1MSa/s
	( 873813.3125,	  50e-3 ),	# mystery clock divider
	( 436906.65625,  100e-3 ),	# PLL at 32.768 Hz ?
	( 218453.328125, 200e-3 ),	# *80/3 yields 873813.3333... Hz
	(  87381.335938, 500e-3 ),
	(  43690.667969,   1 ),
	(  21845.333984,   2 ),
	(   8738.133789,   5 ),
	(   4369.066895,  10 ),
	(   2184.533447,  20 ),
	(    873.813354,  50 ),
    )

    def rate_to_div(self, rate):
	last = None
	for e in self.rate_map:
	    if rate > e[0]:
		break
	    last = e
	if last is None:
	    raise hell
	return last[0], last[1]

    def window(self, rate, t0 = None, t1 = None, trigger = 0):
	real_rate, div = self.rate_to_div(rate)
	if t0 is not None and t1 is not None:
	    if t0 >= t1:
		raise hell
	    samples = (t1-t0)*real_rate
	    if samples > 524288:	# 512*1024 Sa
		raise hell
	self.hor.pos = 0
	self.hor.scale = div
	if t0 is None and t1 is None:
	    self.hor.pos = 0
	else:
	    if t0 is None:
		t0 = t1-self.samples/real_rate
	    center = (t0+t1)/2.0
	    self.hor.pos = center-trigger


class tektronix_mso4000(scope):
    channels = 4
    div_hor = 10
    div_vert = 10
    samples_per_div = 50	# ???

    def __init__(self):
	scope.__init__(self, "usbtmc", "timeout=10", "retry",
          "vendor=0x0699", "product=0x0401")
	self.ch = []
	self.d = map(lambda x: x, range(0, 16))
	for n in range(1, self.channels+1):
	    self.ch.append(channel(self, n))
	self.trigger = None
	self.hor = horizontal(self)
	self.lock_attr()

# for now, we treat it almost like a Rigol

    def forget(self):
	for ch in self.ch:
	    ch.forget()
	self.hor.forget()

    def download_wave(self, channel, start, end, step):
	self.send(":DATA:SOU CH"+str(channel.number))
	self.send(":DATA:ENC RPB")
	self.send(":WFMO:BYT_NR 1")
	self.send(":CURVE B")
	s = self.query(":WFMO?")
	info = s.split(";")
	pts = info[6]
	hincr = float(info[9])
	hzero = float(info[10])
	vincr = float(info[13])
	vzero = float(info[14])
	start = 400000
	self.send(":DATA:START "+str(start))
	self.send(":DATA:STOP 700000")
	d = self.query(":CURVE?")
	digits = int(d[1])
	d = d[2+digits:-1]
	wave = analog()
	t = hzero+hincr*start
	for c in d:
	    wave.append(t, (ord(c)-vzero)*vincr)
	    t += hincr
	return wave

    def sampling_rate(self):
	return float(self.query(":ACQ:SAMP? CH1"))

    def screendump(self):
	self.send(":SAV:IMAG:FILEF PNG");
	return self.query(":HARDCOPY START")

    def wave(self, channels, start = None, end = None, step = None):
	if not hasattr(channels, "__iter__"):
	    return self.wave([channels], start, end, step)[0]
	la = None
	res = waves()
	for ch in channels:
	    if isinstance(ch, channel):
		res.append(self.download_wave(ch, start, end, step))
		res[-1].label = ch.name
	    else:
		if la is None:
		    la = self.download_la(start, end, step)
		res.append(la[ch])
		res[-1].label = "D"+str(ch)
	return res
