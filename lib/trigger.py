#
# trigger.py - Trigger description and application
#
# Copyright (C) 2008 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# @@@ The callback logic isn't quite right - we could have multiple users of
# the same trigger settings, so just attaching it to a scope channel is wrong.
#


from tmc.instrument import settable, setting


class coupling:
    DC, AC, HF, LF = range(4)


class trigger(settable):
    coupling_map = [ "DC", "AC", "HF", "LF" ]
    sweep_map = [ "AUTO", "NORMAL", "SINGLE" ]

    def __init__(self):
	self.channel = None
	self.scope = None
	# Warning: "trigger" doesn't lock itself !

    def source(self, channel, coupling = None):
	if self.scope is not None:
	    self.scope.trigger = None
	self.channel = channel
	if channel is None:
	    self.scope = None
	else:
	    self.scope = channel.scope
	    self.scope.trigger = self
	    self.scope.send(":TRIG:MODE "+self.mode)
	    self.scope.send(":TRIG:"+self.mode+":SOUR "+channel.name)
	    if coupling is not None:
		self.scope.send(
		  ":TRIG:"+self.mode+":COUP "+self.coupling_map[coupling])
	    old = self.scope.hor.sweep
	    self.scope.hor.sweep = None
	    self.scope.hor.sweep = old
	    self.update()

    def send(self, *args):
	if self.scope is not None:
	    self.scope.send(*args)

    def query(self, *args):
	if self.scope is None:
	    raise hell
	return self.scope.query(*args)
	
    def set_sweep(self, value):
	if value is not None:
	    self.scope.send(":TRIG:"+self.mode+":SWE "+self.sweep_map[value])

    def get_sweep(self):
	return self.sweep_map.index(
	  self.scope.query(":TRIG:"+self.mode+":SWE?"))


class slope:
    Rising, Falling, Both = range(3)


class edge(trigger):
    mode = "EDGE"
    slope_map = [ "POSITIVE", "NEGATIVE", "BOTH?" ] # @@@

    def __init__(self, **list):
	trigger.__init__(self)
	# @@@WARNING: Agilent use divs as the trigger level unit, not V.
	self.level = setting(self, ":TRIG:"+self.mode+":LEV",
	  lambda x: float(x),
	  lambda x: "%.9f" % x)
	self.slope = setting(self, ":TRIG:"+self.mode+":SLOP",
	  lambda x: self.slope_map.index(x),
	  lambda x: self.slope_map[x])
	self.forget()
	self.lock_attr()
	self.set(**list)

    def forget(self):
	self.level = None
	self.slope = None

    def update(self):
	old = self.level
	self.level = None
	self.level = old
	old = self.slope
	self.slope = None
	self.slope = old

    def apply(self, wave):
	# @@@ only digital waves for now
	# @@@ need to include holdoff time as well
	if self.level <= 0 or self.level >= 1:
	    return []
	if self.slope == slope.Both:
	    return wave.data[1:]
	res = []
	i = int(wave.initial ^ self.slope == slope.Falling)+1
	while i < len(wave.data):
	    res.append(wave.data[i])
	    i += 2
	return res
