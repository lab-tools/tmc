#
# power.py - Power supply control
#
# Copyright (C) 2008 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


#
# TODO:
#
#

from tmc.instrument import setting, instrument


class power(instrument):
    # Warning: "power" doesn't lock itself !
    pass


#
# The mPP-3040D speaks a rather strange dialect of SCPI.
#


def float_or_none(value):
    try:
	return float(value)
    except:
	return None


class leaptronix_mpp3040d(power):

    mode_map = {
      "CV - Constant Voltage." : "CV",
      "CC - Constant Current!" : "CC" }

    def __init__(self, device):
	power.__init__(self, "tty", "bps=9600", "no_dci", device)

	self.on = setting(self, ":OUTPUT",
	  lambda on: on == "ON",
	  lambda on: ("OFF", "ON")[on])

	self.v_max = setting(self, ":VSET",
	  lambda v: float(v.rstrip("V")),
	  args = "#1 ")

	self.i_max = setting(self, ":ISET",
	  lambda i: float(i.rstrip("A")),
	  args = "#1 ")

#	self.v = setting(self, ":VOUT",
#	  lambda v: float_or_none(v.rstrip("V")),
#	  args = "#1 ")

#	self.i = setting(self, ":IOUT",
#	  lambda i: float_or_none(i.rstrip("A")),
#	  args = "#1 ")

	self.mode = setting(self, ":SYS:STATUS",
	  lambda s: self.mode_map.get(s, None))

	self.lock_attr()

    def v(self):
	return float_or_none(self.query(":VOUT? #1").rstrip("V"))

    def i(self):
	return float_or_none(self.query(":IOUT? #1").rstrip("A"))

    def send(self, s):
	power.send(self, s)
	last_read = power.read(self)

    def read(self):
	return self.last_read

    def query(self, s):
	power.send(self, s)
	return power.read(self)
