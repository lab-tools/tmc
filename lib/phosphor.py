#
# phosphor.py - "Phosphor" screen that remembers how often a pixel was drawn
#
# Copyright (C) 2008 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

# @@@ EXPERIMENTAL !

#
# The screen width is in samples. Lines can only go from one sample to the
# next, i.e., from x to x+1. Lines don't include their final point, the
# assumption being that this point will be part of the next line drawn.
#
# There are several line algorithms:
#
# - "saturated" adds one to each pixel visited
#
# - "beam" mimicks the beam on an oscilloscope's CRT by decreasing the
#    intensity linearly with the length of the line, such that the sum of all
#    the pixels drawn for that line is one.
#
# "saturated" produces good-looking images, but makes areas with a high noise
# look more intense than they are. "beam" is more accurate, but has the small
# disadvantage that a lone outlier does not just use a single color.
#
# Colors are encoded as RGB tuples, where each component is in the range 0 to
# 1. The color conversion function is given the value of the pixel, normalized
# into the range 0 to 1.
#
# The following conversion functions are available:
#
# - "linear": this is just a linear greyscale from black to white
#
# - "default": similar to "linear", but the minimum value is 10% and a mild
#   fake gamma correction is applied, so that infrequent events are more
#   visible
#
# - "spectrum": uses a simple color spectrum going from blue to green to red
#
# Finally also the normalization is configurable:
#
# - linear: linear mapping of the range [0, max] to [0, 1]
#
# - logarithmic: logarithmic mapping of the range [ln(min), ln(max)] to [0, 1].
#   Black pixels are excluded from the minimum.
#


from math import log


class norm_linear:

    def __init__(self, min, max):
	self.f = 1.0/max

    def norm(self, v):
	return self.f*v


class norm_logarithmic:

    def __init__(self, min, max):
	self.offset = -log(min)
	self.f = 1.0/(log(max)+self.offset)

    def norm(self, v):
	return self.f*(log(v)+self.offset)


class phosphor(object):

    linear = norm_linear
    logarithmic = norm_logarithmic

    def __init__(self, x, y):
	self.m = []
	for i in range(0, x):
	    self.m.append([0.0]*y)
	self.x = x
	self.y = y

    def saturated_line(self, x, y0, y1):
	if y0 == y1:
	    self.m[x][y0] += 1.0
	elif y0 < y1:
	    for i in range(0, y1-y0):
		self.m[x][y0+i] += 1.0
	else:
	    for i in range(0, y0-y1):
		self.m[x][y0-i] += 1.0

    def beam_line(self, x, y0, y1):
	if y0 == y1:
	    self.m[x][y0] += 1.0
	elif y0 < y1:
	    f = 1.0/(y1-y0)
	    for i in range(0, y1-y0):
		self.m[x][y0+i] += f
	else:
	    f = 1.0/(y0-y1)
	    for i in range(0, y0-y1):
		self.m[x][y0-i] += f

    draw_line = beam_line

    #
    # "draw_window" is where really all the magic happens. It differs from a
    # simple iteration over a subset of the samples by allowing the reference
    # point of the waveform and the reference point on the screen to bet set
    # independently.
    # 
    # "samples" is the list of sample values.
    # "start" is the reference point in the sample list.
    # "offset" is the on-screen position of the reference point. If "offset" is
    # positive, samples before "start" are drawn.
    # 

    def draw_window(self, samples, start = 0, offset = 0):
	for x in range(max(0, offset-start),
	  min(self.x, len(samples)-start+offset)-1):
	    pos = start+x-offset
	    self.draw_line(x, samples[pos], samples[pos+1])

    def color_default(self, v):
	return [0.1+0.9*v*(2.0-v)]*3

    def color_linear(self, v):
	return (v, v, v)

    def color_spectrum(self, v):
	return (
	  max(0.0, (4.0*v-4.0)*(1.0-v)+1.0),
	  max(0.0, 8.0*v*(1.0-v)-1.0),
	  max(0.0, 4.0*v*-v+1.0))
	return (
	  min(1.0, max(0.0, (2.0*v-2.0)*(v+0.5)+1.0)),
	  max(0.0, 16.0*v*(1.0-v)-1.0),
	  min(1.0, 8.0*v*v))

    def max(self):
	return reduce(lambda a, b: max(a, max(b)), self.m, 0)

    def min(self):
	return reduce(lambda a, b: min(a,
	  reduce(lambda a, b: [a, min(a, b)][b != 0], b, 1)),
          self.m, 1)

    def pnm(self, px = 1, py = 1, colors = None, normalize = norm_linear):
	if colors is None:
	    fn = self.color_default
	elif hasattr(colors, "__iter__"):
	    fn = lambda v: colors[min(len(colors)-1, int(v*len(colors)))]
	else:
	    fn = colors

	s = "P6 %d %d 255\n" % (self.x*px, self.y*py)

	norm = normalize(self.min(), self.max())

	for y in range(self.y-1, -1, -1):
	    row = ""
	    for x in range(0, self.x):
		if self.m[x][y] == 0:
		    t = (0, 0, 0)
		else:
		    t = fn(norm.norm(self.m[x][y]))
		t = chr(min(int(t[0]*256), 255))+ \
		  chr(min(int(t[1]*256), 255))+ \
		  chr(min(int(t[2]*256), 255))
		for i in range(0, px):
		    row += t
	    for i in range(0, py):
		s += row
	return s
