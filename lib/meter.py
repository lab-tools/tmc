#
# meter.py - Multimeter control
#
# Copyright (C) 2008, 2009 by OpenMoko, Inc.
# Copyright (C) 2009 by Werner Almesberger
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# async:
#
# - setup measurement
# - start capture
# - notify(capture ends)/stop capture
# - retrieve and process
#
# .start(samples = Null)
# .wait -> return vector
# .stop -> return vector
#


from tmc.instrument import instrument, setting, settings


class meter(instrument):
    def __init__(self, *args):
	# Warning: "meter" doesn't lock itself !
	instrument.__init__(self, *args)

	self.stat = setting(self, "CALC:FUNC AVER;:CALC:STAT",
	  lambda x: x == 1,
	  lambda x: ("OFF", "ON")[x != 0])
	self.min = setting(self, "CALC:AVER:MIN",
	  lambda x: float(x), None)
	self.max = setting(self, "CALC:AVER:MAX",
	  lambda x: float(x), None)
	self.avg = setting(self, "CALC:AVER:AVER",
	  lambda x: float(x), None)
	self.count = setting(self, "CALC:AVER:COUNT",
	  lambda x: int(x), None)

    def sample(self):
	return float(self.query("READ?"))

    def average(self, n):
	sum = 0
	for i in range(0, n):
	    sum += self.sample()
	return sum/n

    def v(self, range = None):
	if range is None:
		self.send("CONF:VOLT:DC")
	else:
		self.send("CONF:VOLT:DC "+str(range))

    def i(self, range = None):
	if range is None:
		self.send("CONF:CURR:DC MAX")
	else:
		self.send("CONF:CURR:DC "+str(range))


class fluke_8845a(meter):

    def __init__(self, name):
	meter.__init__(self, "telnet", "tries=3", "timeout=5", name)
	self.lock_attr()
# Reset often activates some relays, so we shouldn't use it carelessly.
#	self.send("*RST")
	self.send(":SYST:REM")


class m3005a_chan(settings):

    def __init__(self, instr):
	self.curr = None
	self.front = None
	self.scanner = None
	self.instr = instr

    def is_front(self):
	if self.front is None:
	    term = self.instr.query("ROUT:TERM?")
	    if term == "FRON":
		self.front = True
	    elif term == "REAR":
		self.front = False
	    else:
		raise hell
	return self.front

    def has_scanner(self):
	if self.scanner is None:
	    term = self.instr.query("ROUT:STAT?")
	    if term == "1":
		self.scanner = True
	    elif term == "0":
		self.scanner = False
	    else:
		raise hell
	return self.scanner

    def get(self):
	if self.curr is None:
	    if self.is_front():
		self.curr = 0
	    else:
		if self.has_scanner():
		    closed = self.instr.query("ROUT:CLOS?")
		    self.curr = None
		    n = 1
		    for ch in closed:
			if ch == "1":
			    if self.curr is not None:
				raise hell
			    self.curr = n
			if ch == ",":
			    n += 1
		else:
		    self.curr = 1
	return self.curr

    def set(self, value):
	if self.curr is not None and self.curr == value:
	    return
	if self.is_front():
	    if value != 0:
		raise hell
	else:
	    if value < 1 or value > 20:
		raise hell
	    if self.has_scanner():
		self.instr.send("ROUT:OPEN")
		self.instr.send("ROUT:CLOS "+str(value))
	    else:
		if value > 1:
		    raise hell
	self.curr = value


class picotest_m3500a(meter):

    def __init__(self):
	meter.__init__(self, "usbtmc", "timeout=2", "retry", "vendor=0x164e",
	  "product=0x0dad")
	self.send("*RST")
	self.chan = m3005a_chan(self)
	self.lock_attr()
