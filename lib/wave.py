#
# wave.py - Analog and digital waveforms
#
# Copyright (C) 2008, 2009 by OpenMoko, Inc.
# Written by Werner Almesberger <werner@openmoko.org>
# All Rights Reserved
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

# waveform: (t, v)*
# delta-waveform (digital): (s0, t*)
# events: t*
# samples: v*

# digitize: (t, v)*  ->  (s0, t*)

# clock extraction:
#  rising   (s0, t*)  ->  t*
#  falling  (s0, t*)  ->  t*
#  both     (s0, t*)  ->  t*

# clock arithmetic:
#  divide   t*  ->  t*
#  multiply (divide interval)  t* -> t*
#       monoflop  t* -> t*
#  PLL  t* -> t*

# clock recovery:
#  implicit t*  ->  t*

# sample: (t, v)*  x  t*  ->  v*
#     or  (s0, t*)  x  t*  ->  v*

# de-interpolation (analog): remove points that can be interpolated


#
# Known restrictions:
# - waves.load has no way of knowing what kind of wave it is loading, so it
#   just makes everything an analog wave. Should perhaps add a comment on top
#   of the file with same hints. Or let provide a means for the user to tell
#   waves.load what the waves are like. An even more radical approach may be
#   to automatically convert waves consisting only of 0 and 1 and with only
#   vertical transitions to digital. CPU time is cheap :-)
#

import re


# === Waves ===================================================================


class wave(object):

    def save(self, name, append = False):
	f = open(name, ("w", "a")[append])
	if append:
	    print >>f
	if self.label is not None:
	    print >>f, "# TMC-Label:", self.label
	for xy in self:
	    print >>f, xy[0], xy[1]
	f.close()

    def load(self, name, step = None):
	self.__init__()
	empty = re.compile("^\s*(#.*)$")
	# note: an empty label counts as no label
	label = re.compile("^#\s*TMC-Label:\s+(\S+)\s*$")
	if step is None:
	    numbers = re.compile("^\s*(\S+)\s*(\S+)\s*")
	else:
	    numbers = re.compile("^\s*(\S+)?\s*(\S+)\s*")
	    t = 0
	f = open(name, "r")
	while True:
	    line = f.readline()
	    if line == "":
		break
	    if empty.match(line):
		m = label.match(line)
		if m is not None:
		    self.label = m.group(1)
		continue
	    m = numbers.match(line)
	    if m is None:
		raise hell
	    if step is None:
		self.append(float(m.group(1)), float(m.group(2)))
	    else:
		self.append(t, float(m.group(2)))
		t += step
	f.close()

    def get(self, t):
	if hasattr(t, "__iter__"):
	    res = []
	    for v in t:
		res.append(self.get_one(v))
	    return res
	else:
	    return self.get_one(t)

    def unary(self, op):
	res = analog()
	res.label = self.label
	for p in self:
	    res.append(float(p[0]), float(op(p[1])))
	return res

    def binary(self, other, op):
	res = analog()
	if isinstance(self, wave):
	    s = self.label
	    if not s:
		s = ""
	    if isinstance(other, wave):
		if False: # other.label:
		    if s:
			s += "_"
		    s += other.label
		for v in waves(self, other).iterate():
		    res.append(v[0], op(float(v[1]), float(v[2])))
	    else:
		for p in self:
		    res.append(p[0], op(float(p[1]), float(other)))
	else:
	    s = other.label
	    for p in other:
		res.append(p[0], op(float(self), float(p[1])))
	res.label = s
	return res

    def __add__(self, other):
	return self.binary(other, float.__add__)

    def __sub__(self, other):
	return self.binary(other, float.__sub__)

    def __mul__(self, other):
	return self.binary(other, float.__mul__)

    def __pow__(self, other):
	return self.binary(other, float.__pow__)

    def __div__(self, other):
	return self.binary(other, float.__div__)

    def __truediv__(self, other):
	return self.binary(other, float.__truediv__)

    def __radd__(self, other):
	return self.binary(other, float.__radd__)

    def __rsub__(self, other):
	return self.binary(other, float.__rsub__)

    def __rmul__(self, other):
	return self.binary(other, float.__rmul__)

    def __rpow__(self, other):
	return self.binary(other, float.__rpow__)

    def __rdiv__(self, other):
	return self.binary(other, float.__rdiv__)

    def __rtruediv__(self, other):
	return self.binary(other, float.__rtruediv__)

    def __neg__(self):
	return self.unary(float.__neg__)

    def __abs__(self):
	return self.unary(float.__abs__)


# === Wave groups =============================================================


def next_or_none(iter):
    try:
	return iter.next()
    except StopIteration:
	return None


# The iteration is a bit tricky, because waves may have different sample
# intervals, so we need to interpolate, and they may have multiple samples with
# the same time (in the case of digital waves), so we need to make sure we stop
# at a given time until all samples have been extraced.

class waves_iter:

    def __init__(self, waves):
	self.iter = []
	self.cur = []
	self.nxt = []
	self.t = None
	for w in waves:
	    i = iter(w)
	    v = next_or_none(i)
	    self.iter.append(i)
	    self.nxt.append(v)
	    self.cur.append(None)
	    if self.t is None:
		if v is not None and (self.t is None or v[0] < self.t):
		    self.t = v[0]

    def __iter__(self):
	return self

    def next(self):
	if self.t is None:
	    raise StopIteration
	next_t = None
	res = [ self.t ]
	for i in range(0, len(self.cur)):
	    if self.nxt[i] is not None and self.nxt[i][0] == self.t:
		self.cur[i] = self.nxt[i]
		self.nxt[i] = next_or_none(self.iter[i])
	    if self.nxt[i] is not None and \
	      (next_t is None or self.nxt[i][0] < next_t):
		next_t = self.nxt[i][0]
	    if self.cur[i] is None:
		if self.nxt[i] is None:
		    v = 0
		else:
		    v = self.nxt[i][1]
	    else:
		if self.nxt[i] is None or self.cur[i][0] == self.t:
		    v = self.cur[i][1]
		else:
		    v = self.cur[i][1]+(self.nxt[i][1]-self.cur[i][1])* \
		      (self.t-self.cur[i][0])/ \
		      float(self.nxt[i][0]-self.cur[i][0])
	    res.append(v)
	self.t = next_t
	return res


class waves(list):

    def __init__(self, *args):
	list.__init__(self)
	self.extend(args)

    def iterate(self):
	return waves_iter(self)

    def save(self, name, append = False):
	f = open(name, ("w", "a")[append])
	if append:
	    print >>f

	s = ""
	for w in self:
	    if s:
		s += ", "
	    if w.label is not None:
		s += w.label
	if s:
	    print >>f, "# TMC-Label:", s

	for v in self.iterate():
	    for i in range(0, len(v)-1):
		print >>f, v[i],
	    print >>f, v[-1]

	f.close()

    def load(self, name, step = None):
	list.__init__(self)
	empty = re.compile("^\s*(#.*)$")
	label = re.compile("^#\s*TMC-Label:\s+((\S*\s*,\s*)*\S*)?\s*$")
	space = re.compile("\s+")
	first = True
	skip = step is None
	t = 0
	labels = None
	f = open(name, "r")
	while True:
	    line = f.readline()
	    if line == "":
		break
	    if empty.match(line):
		m = label.match(line);
		if m is not None:
		    labels = m.group(1).split(",")
		continue
	    m = space.split(line.lstrip().rstrip())
	    if first:
		for i in range(0, len(m)-skip):
		    self.append(analog())
		first = False
	    else:
		if len(m)-skip != len(self):
		    raise hell
	    if step is None:
		for i in range(0, len(m)-1):
		    self[i].append(float(m[0]), float(m[i+1]))
	    else:
		for i in range(0, len(m)):
		    self[i].append(t, float(m[i]))
		t += step
	f.close()
	if labels is None:
	    return
	one_label = re.compile("\s*(\S+)\s*")
	for i in range(0, len(labels)):
	    m = one_label.match(labels[i])
	    if m is not None:
		self[i].label = m.group(1)


# === Analog waves ============================================================


class analog_iter:

    def __init__(self, wave):
	self.wave = wave
	self.pos = 0

    def next(self):
	if self.pos == len(self.wave.data):
	    raise StopIteration
	res = self.wave.data[self.pos]
	self.pos += 1
	return res


#
# This is a special binary search: if there's no exact match, we return the
# index of the element preceding the value. If the value is less than the first
# element, we return -1.
#

def binary(list, item, find):
    low = 0
    high = len(list)
    while low != high:
	mid = (low+high) >> 1
	if item(list[mid]) < find:
	    low = mid+1
	else:
	    high = mid
    if low == len(list):
	return low-1
    if item(list[low]) > find:
	return low-1
    return low


class analog(wave):

    def __init__(self):
	self.data = []
	self.label = None

    def start(self):
	if len(self.data) == 0:
	    return None
	return self.data[0][0]

    def end(self):
	if len(self.data) == 0:
	    return None
	return self.data[-1][0]

    def append(self, t, y):
	if len(self.data):
	    if t < self.data[-1][0]:
		raise hell
	    if t == self.data[-1][0] and y == self.data[-1][1]:
		return
	self.data.append((t, y))

    def extend(self, wave):
	if len(self.data) and len(wave.data) and \
	  wave.data[0][0] < self.data[-1][0]:
	    raise hell
	self.data.extend(wave.data)

    def index(self, t):
	if len(self.data) == 0 or t < self.data[0][0] or t > self.data[-1][0]:
	    raise hell
	return binary(self.data, lambda x: x[0], t)

    def get_one(self, t):
	return self.data[self.index(t)]

    def __iter__(self):
	return analog_iter(self)

    def __len__(self):
	return len(self.data)

    def digitize(self, low, high = None):
	if high is None:
	    high = low
	if high < low:
	    raise hell
	res = digital()
	res.label = self.label
	for p in self.data:
	    if p[1] >= high:
		res.append(p[0], 1)
	    elif p[1] <= low:
		res.append(p[0], 0)
	return res


# === Digital waves ===========================================================


class digital_iter:

    def __init__(self, wave):
	self.wave = wave
	self.pos = 0
	self.risen = True

    def next(self):
	if self.pos > len(self.wave.data):
	    raise StopIteration
	elif self.pos == len(self.wave.data):
	    self.pos += 1
	    return (self.wave.t_end,
	      int((len(self.wave.data) & 1) ^ (not self.wave.initial)))
	res = (self.wave.data[self.pos],
	  int((self.pos & 1) ^ self.wave.initial ^ (not self.risen)))
	if self.risen:
	    self.pos += 1
	self.risen = not self.risen
	return res


class digital(wave):

    def __init__(self):
	self.initial = None
	self.data = []
	self.t_end = None
	self.label = None

    def start(self):
	if self.initial is None:
	    return None
	return self.data[0]

    def end(self):
	return self.t_end

    def append(self, t, y):
	if self.t_end is not None and t < self.t_end:
	    raise hell
	if self.initial is None:
	    self.initial = not not y
	if self.initial == (not not y, not y)[len(self.data) & 1]:
	    self.data.append(t)
	self.t_end = t

    def extend(self, wave):
	if wave.t_end is None:
	    return
	if self.t_end is None:
	    self.t_end = wave.t_end
	    self.data = wave.data
	    self.initial = wave.initial
	    return
	if wave.data[0] < self.t_end:
	    raise hell
	if self.initial ^ (len(self.data) & 1) == wave.initial:
	    self.data.extend(wave.data)
	else:
	    self.data.extend(wave.data[1:])
	self.t_end = wave.t_end

    def index(self, t):
	if len(self.data) == 0 or t < self.data[0] or t > self.t_end:
	    raise hell
	return binary(self.data, lambda x: x, t)

    def get_one(self, t):
	if len(self.data) == 0 or t < self.data[0] or t > self.t_end:
	    raise hell
	return int(self.initial ^ (self.index(t) & 1))

    # experimental

    def sample_step(self, tolerance = 0.01):
	if len(self.data) < 2:
	    return None
	min = self.data[1]-self.data[0]
	for i in range(1, len(self.data)-1):
	    if min < self.data[i+1]-self.data[i]:
		min = self.data[i+1]-self.data[i]
	div = 1
	while True:
	    step = min/float(div)
	    fuzz = step*tolerance
	    for t in self.data:
		d = t-self.data[0]
		n = round(d/step)
		if abs(d-n*step) > fuzz:
		    break
	    else:
		return step
	    div += 1

    # experimental

    def sample(self, step = None, tolerance = 0.01):
	if len(self.data) == 0:
	    return []
	if len(self.data) == 1:
	    if step is None:
		return [int(self.initial)]
	    return [int(self.initial)]* \
	      int(round((self.t_end-self.data[0])/float(step)))
	if step is None:
	    step = self.sample_step()
	value = int(self.initial)
	last = self.data[0]
	res = []
	for t in self.data[1:]:
	    n = round((t-last)/step)
	    if abs(n*step-(t-last)) > step*tolerance:
		raise hell
	    res.extend([value]*int(n))
	    last = t
	    value = 1-value
	res.extend([value]*int((self.end()-t)/step+0.5))
	return res

    def __iter__(self):
	return digital_iter(self)

    def debounce(self, t):
	i = 0
	while i < len(self.data)-1:
	    if self.data[i]+t <= self.data[i+1]:
		i += 1
	    else:
		# If we begin in a glitch, we just remove the second entry,
		# so that the start of the waveform is preserved.
		if i == 0:
		    del self.data[1]
		    self.initial = not self.initial
		else:
		    del self.data[i]
		    del self.data[i]

    def __len__(self):
	return len(self.data)*2
