/*
 * python.c - Python binding for TMC functions
 *
 * Copyright (C) 2008, 2009 by OpenMoko, Inc.
 * Written by Werner Almesberger <werner@openmoko.org>
 * All Rights Reserved
 *
 * Continued 2014 by Werner Almesberger <werner@almesberger.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <Python.h>

#include "tmc.h"


#define BUF_SIZE (21*1024*1024)	/* Tek MSO4000 can return about 20 MB */
#define ERROR fprintf(stderr, "ERROR %d\n", __LINE__)
	/* @@@FIXME: raise exceptions */

struct py_instr {
	PyObject_HEAD
	struct tmc_dsc *instr;
};


static char *buf;


/*
 * @@@FIXME: how to _properly_ deallocate these strings ?
 */

static char **make_argv(int skip, PyObject *args, int *argc)
{
	char **argv;
	PyObject *obj;
	int i;

	*argc = PyTuple_Size(args)-skip;
	if (*argc < 0)
		return NULL;
	argv = malloc(sizeof(char *)* *argc);
	if (!argv) {
		PyErr_NoMemory();
		return NULL;
	}
	for (i = 0; i != *argc; i++) {
		obj = PyTuple_GetItem(args, skip+i);
		argv[i] = PyString_AsString(obj);
	}
	return argv;
}


static PyObject *tmc_tp_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
	struct py_instr *s;

fprintf(stderr, "tmc_tp_new\n");
	s = (struct py_instr *) type->tp_alloc(type, 0);
	if (s)
		s->instr = NULL;
	return (PyObject *) s;
}


static int tmc_tp_init(PyObject *self, PyObject *args, PyObject *kwds)
{
	struct py_instr *s = (struct py_instr *) self;
	char **argv;
	int argc;
	const struct proto_ops *ops;

	if (s->instr) {
		ERROR;
		fprintf(stderr, "tmc_tp_init: already initialized\n");
		return -1;
	}
	argv = make_argv(0, args, &argc);
	if (!argv) {
		ERROR;
		return -1;
	}
	if (!argc) {
		ERROR;
		free(argv);
		return -1;
	}
	ops = tmc_ops(*argv);
	if (!ops) {
		ERROR;
		free(argv);
		return -1;
	}
	s->instr = tmc_open(ops, argc-1, (const char **) argv+1);
	free(argv);
	if (s->instr)
		return 0;
	ERROR;
	return -1;
}


static void tmc_tp_dealloc(PyObject *self)
{
	struct py_instr *s = (struct py_instr *) self;

	if (s->instr)
		tmc_close(s->instr);
}


static PyObject *tmc_py_send(PyObject *self, PyObject *args)
{
	struct py_instr *s = (struct py_instr *) self;
	char **argv;
	int argc;

//fprintf(stderr, "tmc_py_send\n");
	if (!s->instr) {
		ERROR;
		return NULL;
	}
	argv = make_argv(0, args, &argc);
	if (!argv) {
		ERROR;
		return NULL;
	}
	if (tmc_send(s->instr, argc, (const char **) argv)) {
		ERROR;
		return NULL;
	}
	free(argv);
	return Py_BuildValue("");
}


static PyObject *tmc_py_read(PyObject *self, PyObject *noarg)
{
	struct py_instr *s = (struct py_instr *) self;
	int len;

//fprintf(stderr, "tmc_py_read\n");
	if (!s->instr) {
		ERROR;
		return NULL;
	}
	len = tmc_read(s->instr, buf, BUF_SIZE);
	if (len < 0) {
		ERROR;
		return NULL;
	}
	return Py_BuildValue("s#", buf, len);
}


static PyObject *tmc_py_start(PyObject *self, PyObject *args)
{
	struct py_instr *s = (struct py_instr *) self;
	char **argv;
	int argc;

	if (!s->instr) {
		ERROR;
		return NULL;
	}
	argv = make_argv(0, args, &argc);
	if (!argv) {
		ERROR;
		return NULL;
	}
	if (argc < 1 || argc > 2) {
		ERROR;
		fprintf(stderr, "usage: filename [, command]\n");
		free(argv);
		return NULL;
	}
	if (tmc_start(s->instr, argv[0], argc == 2 ? argv[1] : NULL)) {
		ERROR;
		return NULL;
	}
	free(argv);
	return Py_BuildValue("");
}


static PyObject *tmc_py_stop(PyObject *self, PyObject *noarg)
{
	struct py_instr *s = (struct py_instr *) self;

	if (!s->instr) {
		ERROR;
		return NULL;
	}
	if (tmc_stop(s->instr)) {
		ERROR;
		return NULL;
	}
	return Py_BuildValue("");
}


static PyObject *tmc_py_errors(PyObject *self, PyObject *noarg)
{
	struct py_instr *s = (struct py_instr *) self;
	int errors;

	if (!s->instr) {
		ERROR;
		return NULL;
	}
	errors = tmc_errors(s->instr);
	if (errors < 0) {
		ERROR;
		return NULL;
	}
	return Py_BuildValue("i", errors);
}


static PyObject *tmc_py_debug(PyObject *self, PyObject *arg)
{
	int dbg;

	if (!PyArg_ParseTuple(arg, "i", &dbg))
		return NULL;
	tmc_debug(dbg);
	return Py_BuildValue("");
}


static PyMethodDef tmc_tp_methods[] = {
	{ "send",	tmc_py_send,	METH_VARARGS,	"Send commands" },
	{ "read",	tmc_py_read,	METH_NOARGS,	"Read a response" },
	{ "start",	tmc_py_start,	METH_VARARGS,	"Start async logging" },
	{ "stop",	tmc_py_stop,	METH_NOARGS,	"Stop async logging" },
	{ "errors",	tmc_py_errors,	METH_NOARGS,	"Return error count" },
	{ NULL, NULL, 0, NULL }
};


static PyTypeObject tmc_instr_type = {
	PyObject_HEAD_INIT(NULL)
	.tp_name	= "_tmc.Instr",
	.tp_basicsize	= sizeof(struct py_instr),
	.tp_flags	= Py_TPFLAGS_DEFAULT,
	.tp_doc		= "TMC objects",
	.tp_new		= tmc_tp_new,
	.tp_init	= tmc_tp_init,
	.tp_dealloc	= tmc_tp_dealloc,
	.tp_methods	= tmc_tp_methods,
};


static PyMethodDef tmc_methods[] = {
	{ "debug",	tmc_py_debug,	METH_VARARGS,	"Enable debug output" },
	{ NULL, NULL, 0, NULL }
};


PyMODINIT_FUNC init_tmc(void)
{
	PyObject *m;

	tmc_instr_type.tp_new = PyType_GenericNew;
	if (PyType_Ready(&tmc_instr_type) < 0)
		return;
	m = Py_InitModule3("_tmc", tmc_methods, "Test & Measurement Control");
	Py_INCREF(&tmc_instr_type);
	PyModule_AddObject(m, "Instr", (PyObject *) &tmc_instr_type);
	buf = malloc(BUF_SIZE);
	if (!buf) {
		perror("malloc");
		exit(1);
	}
}
