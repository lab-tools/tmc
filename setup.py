from distutils.core import setup, Extension

setup(name = "tmc",
    version = "0.0",
    description = "Test and Measurement Control",
    py_modules = [ "tmc.instrument",
	"tmc.wave", "tmc.trigger", "tmc.shape",
	"tmc.crc", "tmc.decode", "tmc.dxplore",
	"tmc.phosphor",
        "tmc.meter", "tmc.scope", "tmc.power", "tmc.function" ],
    package_dir = { "tmc": "lib" },
    ext_modules = [
	Extension("_tmc",
            [ "python.c", "tmc.c", "io.c", "telnet.c", "usbtmc.c", "tty.c" ],
            extra_compile_args = [ "-Wall" ],
	    libraries = [ "usb" ]) ])
