.PHONY:		all install system-setup clean spotless

all:		tmc.so

tmc.so:		setup.py python.c tmc.c io.c telnet.c usbtmc.c
		python setup.py build
		find build -name tmc.so -exec mv '{}' . \;

install:
		python setup.py install

# Should perhaps use Debian's addgroup, which gives us better control over the
# GID selected, but that one is lacking the rather convenient -f option.

system-setup:
		groupadd -f usbtmc
		install -m 0644 etc/40-usbtmc-permissions.rules \
		  /etc/udev/rules.d
		/etc/init.d/udev restart

clean:

spotless:
		rm -f tmc.so
