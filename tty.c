/*
 * tty.c - Send and receive SCPI messages over a TTY device
 *
 * Copyright (C) 2008 by OpenMoko, Inc.
 * Written by Werner Almesberger <werner@openmoko.org>
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>

#include "io.h"


#define BUF_SIZE 1024


struct tty_dsc {
	int fd;		/* file descriptor */
	int crlf;	/* 0 = in message, 1 = CR seen, 2 = CRLF seen */
	int echo;	/* device echos commands back */
	int dci;	/* device support DCI */
	struct termios attr;
};


static int bps_to_speed(speed_t *speed, unsigned long bps)
{
	static struct {
		speed_t speed;
		int bps;
	} table[] = {
		{ B300,	      300 },
		{ B1200,     1200 },
		{ B2400,     2400 },
		{ B9600,     9600 },
		{ B19200,   19200 },
		{ B38400,   38400 },
		{ B57600,   57600 },
		{ B115200, 115200 },
		{      0,       0 }
	}, *p;

	for (p = table; p->bps; p++)
		if (p->bps == bps) {
			*speed = p->speed;
			return 0;
		}
	fprintf(stderr, "unknown speed: %lubps\n", bps);
	return -1;
}


static void *tty_open(int argc, const char **argv)
{
	struct tty_dsc *d;
	unsigned long bps = 0;
	speed_t speed = 0; /* initialize to keep gcc from complaining */
	int crtscts = 0;
	char *end;
	int i;
	struct termios attr;

	d = malloc(sizeof(struct tty_dsc));
	if (!d) {
		perror("malloc");
		return NULL;
	}
	d->crlf = 0;
	d->echo = 0;
	d->dci = 1;
	for (i = 0; i != argc; i++) {
		if (!strcmp(argv[i], "crtscts"))
			crtscts = 1;
		else if (!strcmp(argv[i], "echo"))
			d->echo = 1;
		else if (!strcmp(argv[i], "no_dci"))
			d->dci = 0;
		else if (!strncmp(argv[i], "bps=", 4)) {
			bps = strtoul(argv[i]+4, &end, 0);
			if (*end)
				goto usage;
			if (bps_to_speed(&speed, bps) < 0)
				goto usage;
		}
		else if (strchr(argv[i], '='))
			goto usage;
		else
			break;
	}
	if (i != argc-1)
		goto usage;


	d->fd = open(argv[i], O_RDWR | O_NOCTTY, 0);
	if (d->fd < 0) {
		perror(argv[1]);
		goto fail;
	}

	if (tcgetattr(d->fd, &d->attr) < 0) {
		perror("tcgetattr");
		goto fail_fd;
	}
	attr = d->attr;
	cfmakeraw(&attr);
	if (bps &&
	    (cfsetispeed(&attr, speed) < 0 || cfsetospeed(&attr, speed) < 0)) {
		perror("cfsetispeed");
		goto fail_fd;
	}
	attr.c_cflag = (attr.c_cflag & ~CRTSCTS) | (crtscts ? CRTSCTS : 0);
	if (tcsetattr(d->fd, TCSAFLUSH, &attr) < 0) {
		perror("tcsetattr");
		goto fail_fd;
	}
	return d;

usage:
	fprintf(stderr,
	    "usage: \"tty\", [crtscts,] [bps=N,] [echo,] [no_dci,] device\n");
	free(d);
	return NULL;

fail_fd:
	if (close(d->fd) < 0)
		perror("close");
fail:
	free(d);
	return NULL;
}


static void tty_close(void *dsc)
{
	struct tty_dsc *d = dsc;

	if (tcsetattr(d->fd, TCSAFLUSH, &d->attr) < 0)
		perror("tcsetattr");
	if (close(d->fd) < 0)
		perror("close");
}


static int do_write(int fd, const void *buf, size_t len)
{
	ssize_t wrote;

	while (len) {
		wrote = write(fd, buf, len);
		if (wrote < 0) {
			perror("write");
			return -1;
		}
		if (!wrote) {
			fprintf(stderr, "write: wrote 0 bytes\n");
			return -1;
		}
		buf += wrote;
		len -= wrote;
	}
	return 0;
}


/*
 * We carefully check the echo to make sure nothing evil has happened to the
 * command we just sent. E.g., if this is a power supply, we wouldn't want it
 * to output, say, 9.2V instead of 1.2V, just because a bit was flipped.
 */


static int expect(int fd, const void *msg, size_t len)
{
	char buf[BUF_SIZE];
	ssize_t got;

	while (len) {
		got = read(fd, buf, sizeof(buf));
		if (got < 0) {
			perror("read");
			return -1;
		}
		if (!got) {
			fprintf(stderr, "read: unexpected EOF\n");
			return -1;
		}
		if (got > len) {
			fprintf(stderr, "got %d, expected no more than %d\n",
			    (int) got, (int) len);
			return -1;
		}
		if (memcmp(buf, msg, got)) {
			fprintf(stderr, "echo does not match message\n");
			return -1;
		}
		msg += got;	
		len -= got;
	}
	return 0;
}


static int tty_write(void *dsc, const void *buf, size_t len)
{
	struct tty_dsc *d = dsc;

	if (do_write(d->fd, buf, len) < 0)
		return -1;
	if (d->echo && expect(d->fd, buf, len) < 0)
		return -1;
	if (do_write(d->fd, "\r\n", 2) < 0)
		return -1;
	if (d->echo && expect(d->fd, "\r\n", 2) < 0)
		return -1;
	return 0;
}


static int tty_read(void *dsc, io_push_fn *push, void *push_dsc)
{
	struct tty_dsc *d = dsc;
	char buf[BUF_SIZE];
	struct timeval tv;
	ssize_t got;
	char *p;

	do {
		got = read(d->fd, buf, BUF_SIZE);
		if (got < 0) {
			perror("read");
			return -1;
		}
		if (!got) {
			fprintf(stderr, "read: unexpected EOF\n");
			return -1;
		}
		if (gettimeofday(&tv, NULL) < 0) {
			perror("gettimeofday");
			return -1;
		}
		for (p = buf; p != buf+got; p++)
			switch (*p) {
			case '\r':
				if (d->crlf)
					fprintf(stderr,
					    "warning: unexpected \\r\n");
				d->crlf = 1;
				break;
			case '\n':
				if (d->crlf != 1)
					fprintf(stderr,
					    "warning: unexpected \\n\n");
				d->crlf = 2;
				break;
			default:
				if (d->crlf)
					fprintf(stderr,
					    "warning: data after \\r\\n\n");
				d->crlf = 0;
				break;
			}
		if (d->crlf <= got)
			push(push_dsc, &tv, buf, got-d->crlf, d->crlf == 2);
	}
	while (d->crlf != 2);
	d->crlf = 0;
	return 0;
}


static int tty_dci(void *dsc)
{
	struct tty_dsc *d = dsc;

	return d->dci ? do_write(d->fd, "\003", 1) : 0;
}


struct proto_ops tty_ops = {
	.open	= tty_open,
	.close	= tty_close,
	.read	= tty_read,
	.write	= tty_write,
	.dci	= tty_dci,
};
